#!/usr/bin/env bash
export IMAGE=${CONTAINER_IMAGE}:${CI_BUILD_REF}

export BASE_PATH=${BASE_PATH}
export REDIS_TTL=${REDIS_TTL}
export REDIS_HOST=${REDIS_HOST}
export REDIS_PORT=${REDIS_PORT}
export MONGO_URI=${MONGO_URI}

envsubst < templates/deployK8S.yml > .generated/deployK8S.yml 

cat .generated/deployK8S.yml 

kubectl apply -f .generated/

