import { prop, getModelForClass, Ref } from "@typegoose/typegoose";
import { UserModel } from "./user.model";

export class TweetModel {
  @prop()
  public message: string;

  @prop({ ref: () => UserModel, default: null })
  public user: Ref<UserModel>;

  @prop({ ref: () => UserModel, default: [] })
  public likes: Ref<UserModel>[];

  @prop({ ref: () => UserModel, default: [] })
  public dislikes: Ref<UserModel>[];
}

export const Tweet = getModelForClass(TweetModel);
