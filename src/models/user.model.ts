import { prop, getModelForClass } from "@typegoose/typegoose";

export class UserModel {
  @prop({ required: true, default: "", lowercase: true })
  public email: string;
}

export const User = getModelForClass(UserModel);
