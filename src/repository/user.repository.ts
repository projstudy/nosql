import { DocumentQuery } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { singleton } from "tsyringe";
import { UserRepository } from "../config/depency-injection/repositories";
import { RegisterUserDto } from "../config/types";
import { User, UserModel } from "../models/user.model";

@singleton()
export class UserRepositoryImpl implements UserRepository {
  listUsers(): DocumentQuery<DocumentType<UserModel>[], DocumentType<UserModel>> {
    return User.find();
  }
  createUser(registerUser: RegisterUserDto): Promise<DocumentType<UserModel>> {
    return User.create(registerUser);
  }
  findUserByEmail(email: string): DocumentQuery<DocumentType<UserModel>, DocumentType<UserModel>> {
    return User.findOne({ email: email });
  }
}
