import { singleton } from "tsyringe";
import { UserModel } from "../models/user.model";
import { TweetRepository } from "../config/depency-injection/repositories";
import { Tweet, TweetModel } from "../models/tweet.model";
import { DocumentType } from "@typegoose/typegoose";
import { DocumentQuery, Types } from "mongoose";

@singleton()
export class TweetRepositoryImpl implements TweetRepository {
  findByIdWithReactions(tweetId: string): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>> {
    return Tweet.findById(tweetId).populate("user");
  }
  addReaction(
    tweetId: string,
    userId: Types.ObjectId,
    isLike: boolean,
  ): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>> {
    if (isLike) return Tweet.findByIdAndUpdate(tweetId, { $addToSet: { likes: userId }, $pull: { dislikes: userId } });
    return Tweet.findByIdAndUpdate(tweetId, { $addToSet: { dislikes: userId }, $pull: { likes: userId } });
  }
  findById(id: string): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>> {
    return Tweet.findById(id);
  }
  listUserTweets(userId: string): DocumentQuery<DocumentType<TweetModel>[], DocumentType<TweetModel>> {
    return Tweet.find({ user: userId });
  }
  createTweet(user: UserModel, message: string): Promise<DocumentType<TweetModel>> {
    return Tweet.create({ user: user, message: message, dislikes: [], likes: [] });
  }
}
