import fp from "fastify-plugin";
import swagger, { SwaggerOptions } from "fastify-swagger";
import middie from "middie";
import cors from "fastify-cors";
import sensible from "fastify-sensible";

import { FastifyInstance, FastifyPluginAsync } from "fastify";
import { ProjectException } from "../exceptions/project.exception";
import { UnauthorizedException } from "../exceptions/unauthorized.exception";
import { disconnectMongo } from "../factory/database.factory";
import { NotFoundException } from "../exceptions/notfound.exception";
import { Config } from "../config";

const corsOpts = {
  origin: "*",
  credentials: true,
  methods: ["GET", "PUT", "PATCH", "POST", "DELETE"],
};

const swaggerOpts: SwaggerOptions = {
  routePrefix: Config.fastify.basePath + "/documentation",
  exposeRoute: true,
  swagger: {
    info: {
      title: "NosqlTweeter API",
      version: "1.0.0",
    },
    consumes: ["application/json"],
    produces: ["application/json"],
  },
};

function isValidationError(error): boolean {
  return Array.isArray(error.validation) && error.validation.length > 0;
}

const plugin: FastifyPluginAsync = async (fastify: FastifyInstance) => {
  fastify.addHook("onClose", async () => {
    disconnectMongo();
    return;
  });

  fastify
    .register(cors, corsOpts)
    .register(swagger, swaggerOpts)
    .register(middie)
    .register(sensible)
    .after(() => setErrorHandler(fastify));
};

function setErrorHandler(fastify: FastifyInstance): void {
  /* Handle our custom exceptions. */
  fastify.setErrorHandler((error, _request, reply) => {
    switch (error.constructor) {
      case UnauthorizedException:
        reply.unauthorized();
        break;
      case NotFoundException:
        reply.notFound(error.message);
        break;
      case ProjectException:
        reply.badRequest(error.message);
        break;
      default:
        if (isValidationError(error)) {
          reply.badRequest(error.message);
        } else {
          reply.internalServerError();
          console.log(error);
        }

        break;
    }
  });
}

export default fp(plugin, { name: "generic-plugin" });
