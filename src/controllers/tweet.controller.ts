import { FastifyInstance } from "fastify";
import { container } from "tsyringe";
import { Config } from "../config";
import { TweetService } from "../config/depency-injection/services";
import { CreateTweetDto, PatchTweetDto } from "../config/types";
import { createTweetSchema } from "../validator/tweet/create-tweet.schema";
import { listTweetSchema } from "../validator/tweet/list-tweet.schema";
import { patchTweetSchema } from "../validator/tweet/patch-tweet.schema";
import { readTweetSchema } from "../validator/tweet/read-tweet.schema";

export const autoPrefix = Config.fastify.basePath + "/api/tweets";
export default async function (server: FastifyInstance): Promise<void> {
  const tweetService = container.resolve<TweetService>("TweetService");

  server.get<{ Params: { id: string } }>("/:id", { schema: readTweetSchema }, async (request, _reply) => {
    return await tweetService.readTweet(request.params.id);
  });

  server.patch<{ Params: { id: string }; Body: PatchTweetDto }>(
    "/:id",
    { schema: patchTweetSchema },
    async (request, reply) => {
      reply.status(204);
      await tweetService.patchTweet(request.params.id, request.body);
    },
  );

  server.get<{ Querystring: { email: string } }>("/", { schema: listTweetSchema }, async (request, reply) => {
    reply.status(200);
    return tweetService.listUserTweets(request.query.email);
  });

  server.post<{ Body: CreateTweetDto }>("/", { schema: createTweetSchema }, async (request, reply) => {
    reply.status(201);
    return tweetService.registerTweet(request.body);
  });
}
