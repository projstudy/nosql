import { FastifyInstance } from "fastify";
import { container } from "tsyringe";
import { Config } from "../config";
import { UserService } from "../config/depency-injection/services";
import { RegisterUserDto } from "../config/types";
import { createUserSchema } from "../validator/user/create-user.schema";
import { listUserSchema } from "../validator/user/list-tweet.schema";

export const autoPrefix = Config.fastify.basePath + "/api/users";
export default async function (server: FastifyInstance): Promise<void> {
  const userservice = container.resolve<UserService>("UserService");
  server.get(
    "/",
    {
      schema: listUserSchema,
    },
    async (_request, _relpy) => {
      return userservice.listUser();
    },
  );
  server.post<{ Body: RegisterUserDto }>(
    "/",
    {
      schema: createUserSchema,
    },
    async (request, _relpy) => {
      return userservice.registerUser(request.body);
    },
  );
}
