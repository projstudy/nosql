import { inject, singleton } from "tsyringe";
import { RegisterUserDto } from "../config/types";
import { UserRepository } from "../config/depency-injection/repositories";
import { ProjectException } from "../exceptions/project.exception";
import { UserService } from "../config/depency-injection/services";

@singleton()
export class UserServiceImpl implements UserService {
  constructor(
    @inject("UserRepository")
    private readonly _userRepo: UserRepository,
  ) {}

  async listUser(): Promise<RegisterUserDto[]> {
    const users = await this._userRepo.listUsers();
    return users.map((user) => {
      return { email: user.email };
    });
  }

  async registerUser(registerUser: RegisterUserDto): Promise<{ id: string }> {
    const user = await this._userRepo.findUserByEmail(registerUser.email);
    if (user) {
      throw new ProjectException("Email taken");
    } else {
      const createdUser = await this._userRepo.createUser(registerUser);
      return { id: createdUser.id };
    }
  }
}
