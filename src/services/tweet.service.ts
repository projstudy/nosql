import { inject, singleton } from "tsyringe";
import { CreateTweetDto, PatchTweetDto, ListTweetDto, ReadTweetDto } from "../config/types";
import { TweetRepository, UserRepository } from "../config/depency-injection/repositories";
import { CacheService, TweetService } from "../config/depency-injection/services";
import { NotFoundException } from "../exceptions/notfound.exception";
import { UserModel } from "../models/user.model";
import { DocumentType } from "@typegoose/typegoose";

@singleton()
export class TweetServiceImpl implements TweetService {
  constructor(
    @inject("UserRepository")
    private readonly _userRepo: UserRepository,
    @inject("CacheService")
    private readonly _cacehService: CacheService,
    @inject("TweetRepository")
    private readonly _tweetRepo: TweetRepository,
  ) {}

  async readTweet(tweetId: string): Promise<ReadTweetDto> {
    const tweetCache = (await this._cacehService.readTweet(tweetId)) as ReadTweetDto;
    if (tweetCache) {
      return tweetCache;
    }

    const tweet = await this._tweetRepo.findByIdWithReactions(tweetId);
    if (!tweet) {
      throw new NotFoundException("Tweet not found");
    }
    const readTweetDto = {
      message: tweet.message,
      author: (<DocumentType<UserModel>>tweet.user).email,
      likes: tweet.likes.length,
      dislikes: tweet.dislikes.length,
    };
    await this._cacehService.storeTweet(tweet.id, readTweetDto);
    return readTweetDto;
  }

  async patchTweet(tweetId: string, patchTweetDto: PatchTweetDto): Promise<void> {
    const user = await this._userRepo.findUserByEmail(patchTweetDto.email);
    if (!user) {
      throw new NotFoundException("User not found");
    }
    const tweet = await this._tweetRepo.findById(tweetId);
    if (!tweet) {
      throw new NotFoundException("Tweet not found");
    }
    await this._tweetRepo.addReaction(tweet.id, user.id, patchTweetDto.isLike);
  }

  async listUserTweets(email: string): Promise<ListTweetDto[]> {
    const tweetsCache = (await this._cacehService.readTweet(email)) as ListTweetDto[];
    if (tweetsCache) {
      return tweetsCache;
    }

    const user = await this._userRepo.findUserByEmail(email);
    if (!user) {
      throw new NotFoundException("User not found");
    }

    const tweets = await this._tweetRepo.listUserTweets(user.id);
    const parsedTweets = tweets.map((tweet) => {
      return {
        id: tweet.id,
        message: tweet.message,
      };
    });

    await this._cacehService.storeTweet(email, parsedTweets, true);
    return parsedTweets;
  }

  async registerTweet(createTweetDto: CreateTweetDto): Promise<{ id: string }> {
    const user = await this._userRepo.findUserByEmail(createTweetDto.email);
    if (!user) {
      throw new NotFoundException("User not found");
    }
    const tweet = await this._tweetRepo.createTweet(user, createTweetDto.message);
    await this._cacehService.removeCache(createTweetDto.email);
    return { id: tweet.id };
  }
}
