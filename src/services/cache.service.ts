import { inject, singleton } from "tsyringe";
import { UserRepository } from "../config/depency-injection/repositories";
import { CacheService } from "../config/depency-injection/services";
import { Pojo } from "../config/types";
import Redis, { Redis as RedisInstance } from "ioredis";
import { Config } from "../config";

@singleton()
export class CacheServiceImpl implements CacheService {
  private redis: RedisInstance;
  private readonly TTL = Config.redis.ttl;

  constructor(@inject("UserRepository") private readonly _userRepo: UserRepository) {
    this.redis = new Redis(Config.redis.port, Config.redis.host);
  }

  async removeCache(tweetId: string): Promise<void> {
    await this.redis.del([tweetId]);
  }

  async readTweet(tweetId: string): Promise<Pojo | Pojo[]> {
    const pipeResult = await this.redis.multi().get(tweetId).expire(tweetId, this.TTL).exec();
    const tweetAsString: string = pipeResult[0][1];
    if (tweetAsString) {
      return JSON.parse(tweetAsString);
    }
    return undefined;
  }

  async storeTweet(tweetId: string, pojo: Pojo | Pojo[], keepAlive: boolean): Promise<void> {
    if (keepAlive) {
      await this.redis.set(tweetId, JSON.stringify(pojo));
    } else {
      await this.redis.setex(tweetId, this.TTL, JSON.stringify(pojo));
    }
  }
}
