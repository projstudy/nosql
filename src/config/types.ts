export interface Pojo {
  [key: string]: unknown;
}

export type PatchTweetDto = {
  email: string;
  isLike: boolean;
};

export type ReadTweetDto = {
  message: string;
  author: string;
  likes: number;
  dislikes: number;
};

export type ListTweetDto = {
  id: string;
  message: string;
};

export type CreateTweetDto = {
  email: string;
  message: string;
};

export type RegisterUserDto = {
  email: string;
};
