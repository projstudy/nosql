import { config } from "dotenv";
config();

export const Config = {
  fastify: {
    ip: process.env.FASTIFY_IP ? process.env.FASTIFY_IP : "0.0.0.0",
    port: process.env.FASTIFY_PORT ? Number(process.env.FASTIFY_PORT) : 3000,
    basePath: process.env.BASE_PATH ? process.env.BASE_PATH : "",
  },
  redis: {
    ttl: process.env.REDIS_TTL ? Number(process.env.REDIS_TTL) : 5,
    host: process.env.REDIS_HOST ? process.env.REDIS_HOST : "localhost",
    port: process.env.REDIS_HOST ? Number(process.env.REDIS_PORT) : 6379,
  },
  mongodb: {
    uri: process.env.MONGO_URI ? process.env.MONGO_URI : "mongodb://localhost:27017/",
  },
};
