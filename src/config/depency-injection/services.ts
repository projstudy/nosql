import { container } from "tsyringe";
import { CacheServiceImpl } from "../../services/cache.service";
import { TweetServiceImpl } from "../../services/tweet.service";
import { UserServiceImpl } from "../../services/user.service";
import { CreateTweetDto, PatchTweetDto, ListTweetDto, RegisterUserDto, ReadTweetDto, Pojo } from "../types";

/**
 * Services
 */

export interface CacheService {
  storeTweet(key: string, pojo: Pojo | Pojo[], keepAlive?: boolean): Promise<void>;
  readTweet(key: string): Promise<Pojo | Pojo[]>;
  removeCache(key: string): Promise<void>;
}

export interface TweetService {
  readTweet(tweetId: string): Promise<ReadTweetDto>;
  patchTweet(tweetId: string, patchTweet: PatchTweetDto): Promise<void>;
  listUserTweets(email: string): Promise<ListTweetDto[]>;
  registerTweet(createTweetDto: CreateTweetDto): Promise<{ id: string }>;
}

export interface UserService {
  listUser(): Promise<RegisterUserDto[]>;
  registerUser(registerUser: RegisterUserDto): Promise<{ id: string }>;
}

container.register<UserService>("UserService", { useClass: UserServiceImpl });
container.register<TweetService>("TweetService", { useClass: TweetServiceImpl });
container.register<CacheService>("CacheService", { useClass: CacheServiceImpl });
