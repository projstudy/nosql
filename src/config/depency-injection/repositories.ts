import { DocumentQuery, Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { RegisterUserDto } from "../types";
import { container } from "tsyringe";
import { UserRepositoryImpl } from "../../repository/user.repository";
import { TweetRepositoryImpl } from "../../repository/tweet.repository";
import { UserModel } from "../../models/user.model";
import { TweetModel } from "../../models/tweet.model";

/**
 * Repositories
 */

export interface UserRepository {
  listUsers(): DocumentQuery<DocumentType<UserModel>[], DocumentType<UserModel>>;
  createUser(registerUser: RegisterUserDto): Promise<DocumentType<UserModel>>;
  findUserByEmail(email: string): DocumentQuery<DocumentType<UserModel>, DocumentType<UserModel>>;
}

export interface TweetRepository {
  findByIdWithReactions(id: string): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>>;
  addReaction(
    tweetId: string,
    userId: Types.ObjectId,
    isLike: boolean,
  ): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>>;
  findById(id: string): DocumentQuery<DocumentType<TweetModel>, DocumentType<TweetModel>>;
  listUserTweets(id: string): DocumentQuery<DocumentType<TweetModel>[], DocumentType<TweetModel>>;
  createTweet(user: UserModel, message: string): Promise<DocumentType<TweetModel>>;
}

container.register<UserRepository>("UserRepository", { useClass: UserRepositoryImpl });
container.register<TweetRepository>("TweetRepository", { useClass: TweetRepositoryImpl });
