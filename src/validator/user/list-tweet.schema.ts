import { FastifySchema } from "fastify/types/schema";

export const listUserSchema: FastifySchema = {
  tags: ["Users"],
  summary: "List all registered users",
  response: {
    200: {
      type: "array",
      items: {
        type: "object",
        properties: {
          email: {
            description: "Email",
            type: "string",
            format: "email",
          },
        },
      },
    },
  },
};
