import { FastifySchema } from "fastify/types/schema";

export const createUserSchema: FastifySchema = {
  tags: ["Users"],
  summary: "Register a new user",
  body: {
    type: "object",
    properties: {
      email: {
        description: "Email",
        type: "string",
        format: "email",
      },
    },
    required: ["email"],
  },
  response: {
    201: {
      type: "object",
      properties: {
        id: { type: "string" },
      },
    },
    400: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
    401: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
  },
};
