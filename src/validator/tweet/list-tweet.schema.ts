import { FastifySchema } from "fastify/types/schema";

export const listTweetSchema: FastifySchema = {
  tags: ["Tweets"],
  summary: "Given a email, list all the user's tweets",
  querystring: {
    type: "object",
    properties: {
      email: {
        type: "string",
        format: "email",
      },
    },
    required: ["email"],
  },
  response: {
    201: {
      type: "array",
      items: {
        type: "object",
        properties: {
          email: {
            description: "Email",
            type: "string",
            format: "email",
          },
          message: {
            description: "Message",
            type: "string",
            maxLength: 256,
          },
        },
      },
    },
    400: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
    401: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
  },
};
