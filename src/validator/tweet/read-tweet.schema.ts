import { FastifySchema } from "fastify/types/schema";

export const readTweetSchema: FastifySchema = {
  tags: ["Tweets"],
  summary: "Given an Id, returns the tweet's Likes/Dislike, Message, etc...",
  params: {
    type: "object",
    properties: {
      id: {
        type: "string",
        pattern: "^[0-9a-fA-F]{24}$",
      },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        likes: {
          description: "Number of likes",
          type: "number",
        },
        dislikes: {
          description: "Number of dislikes",
          type: "number",
        },
        author: {
          description: "Author Id",
          type: "string",
        },
        message: {
          description: "Message",
          type: "string",
          maxLength: 256,
        },
      },
    },
    400: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
    401: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
  },
};
