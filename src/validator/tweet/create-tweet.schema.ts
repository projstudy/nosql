import { FastifySchema } from "fastify/types/schema";

export const createTweetSchema: FastifySchema = {
  tags: ["Tweets"],
  summary: "Creates a new tweet",
  body: {
    type: "object",
    properties: {
      email: {
        description: "Email",
        type: "string",
        format: "email",
      },
      message: {
        description: "Message",
        type: "string",
        maxLength: 256,
      },
    },
    required: ["email", "message"],
  },
  response: {
    201: {
      type: "object",
      properties: {
        id: { type: "string" },
      },
    },
    400: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
    401: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
  },
};
