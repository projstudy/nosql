import { FastifySchema } from "fastify/types/schema";

export const patchTweetSchema: FastifySchema = {
  tags: ["Tweets"],
  summary: "Like or dislike a given tweet Id",
  params: {
    type: "object",
    properties: {
      id: {
        type: "string",
      },
    },
  },
  body: {
    type: "object",
    properties: {
      isLike: {
        type: "boolean",
      },
      email: {
        type: "string",
        format: "email",
      },
    },
    required: ["email", "isLike"],
  },
  response: {
    201: {
      type: "array",
      items: {
        type: "object",
        properties: {
          email: {
            description: "Email",
            type: "string",
            format: "email",
          },
          message: {
            description: "Message",
            type: "string",
            maxLength: 256,
          },
        },
      },
    },
    400: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
    401: {
      type: "object",
      properties: {
        error: { type: "string" },
        message: { type: "string" },
        statusCode: { type: "number" },
      },
    },
  },
};
