import "./config/depency-injection";

import { connectMongo } from "./factory/database.factory";
import { FastifyFactory } from "./factory/fastify.factory";
import { Config } from "./config/";
import { container } from "tsyringe";

async function main(): Promise<void> {
  const fastifyFactory = container.resolve(FastifyFactory);
  const server = await fastifyFactory.build();
  const promiseResult = await Promise.all([connectMongo(), server.listen(Config.fastify.port, Config.fastify.ip)]);
  const address = promiseResult[1];
  console.log(`Server running at: ${address}`);
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
