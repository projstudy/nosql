import { join } from "path";
import Fastify, { FastifyInstance } from "fastify";
import AutoLoad from "fastify-autoload";
import Morgan from "morgan";
import { ServerOptions } from "https";
import { singleton } from "tsyringe";

@singleton()
export class FastifyFactory {
  private instance: FastifyInstance;

  async build(options: ServerOptions = {}): Promise<FastifyInstance> {
    this.instance = Fastify({
      ...options,
      ignoreTrailingSlash: true,
    });
    await this.instance.register(AutoLoad, {
      dir: join(__dirname, "../plugins"),
    });
    this.instance.use(Morgan("dev", { skip: (req, _res) => req.url.includes("documentation") }));
    this.instance.register(AutoLoad, {
      dir: join(__dirname, "../controllers"),
    });
    return this.instance;
  }

  async getInstance(): Promise<FastifyInstance> {
    if (!this.instance) {
      await this.build();
    }
    return this.instance;
  }

  async stopServer(): Promise<void> {
    await this.instance.close();
  }
}
