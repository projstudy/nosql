import Mongoose from "mongoose";
import { Config } from "../config";

export const connectMongo = async (): Promise<void> => {
  Mongoose.set("useFindAndModify", false);
  await Mongoose.connect(Config.mongodb.uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "tweetnosql",
  });
};

export const disconnectMongo = (): void => {
  Mongoose.disconnect();
};
