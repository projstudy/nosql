FROM node:12-alpine
WORKDIR /app
ENTRYPOINT ["/sbin/tini", "--"]
RUN apk add --no-cache tini tzdata
ENV TZ=America/Montevideo

COPY package.json ./
COPY ./node_modules ./node_modules
RUN npm prune --production
COPY ./build ./build
CMD ["node","build/app.js"]

